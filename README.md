App Android para controle do radio transceptor Telefunken RTH-220 modificado para uso em bandas de Radio Amador. 

Desenvolvido para utilizar uma API Rest rodando em um Raspberry Pi com o modulo si5351 como oscilador de VFO.

Código da API Rest correspondente encontrado aqui: https://gitlab.com/fabssilveira/raspberry-rest-api-para-vfo-com-si5351.git

Layout criado por: André Felipe Angeloni Rodrigues.