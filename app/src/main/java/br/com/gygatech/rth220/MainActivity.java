package br.com.gygatech.rth220;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import model.Vfo;
import utils.HttpUtils;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences settings;
    private TextView dial;
    private Button btn15m;
    private Button btn20m;
    private Button btn40m;
    private Button btn80m;
    private Button btnLSB;
    private Button btnUSB;
    private Button btnFT8;
    private ImageView imgMinusL;
    private ImageView imgMinusM;
    private ImageView imgMinusS;
    private ImageView imgPlusL;
    private ImageView imgPlusM;
    private ImageView imgPlusS;
    private ImageView imgPlay;
    private AlertDialog dialogUrl;

    private final String BANDA_15M = "15m";
    private final String BANDA_20M = "20m";
    private final String BANDA_40M = "40m";
    private final String BANDA_80M = "80m";

    private String bandaSelecionada = BANDA_40M;
    private String modo = "USB";
    private int freq = 7000000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dial = findViewById(R.id.lblFrequencia);
        btn15m = findViewById(R.id.btn15m);
        btn20m = findViewById(R.id.btn20m);
        btn40m = findViewById(R.id.btn40m);
        btn80m = findViewById(R.id.btn80m);
        btnLSB = findViewById(R.id.btnLSB);
        btnUSB = findViewById(R.id.btnUSB);
        btnFT8 = findViewById(R.id.btnFT8);
        imgMinusL = findViewById(R.id.imgMinusL);
        imgMinusM = findViewById(R.id.imgMinusM);
        imgMinusS = findViewById(R.id.imgMinusS);
        imgPlusL = findViewById(R.id.imgPlusL);
        imgPlusM = findViewById(R.id.imgPlusM);
        imgPlusS = findViewById(R.id.imgPlusS);
        imgPlay = findViewById(R.id.imgPlay);

        settings = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String ip = settings.getString("ip", "192.168.0.93");
        HttpUtils.setEndpoint(ip);

        //não utilizado ainda
        /*dial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculaFrequenciaDoDisplay();
            }
        });*/

        btn15m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selecionaBotao15m();
                bandaSelecionada = BANDA_15M;
                freq = 21000000;
                atualizaDisplay();
                Vfo vfo = new Vfo();
                vfo.setBanda(bandaSelecionada);
                vfo.setFrequencia(freq);
                new DefineFrequencia().execute(vfo);
            }
        });

        btn20m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotao20m();
                bandaSelecionada = BANDA_20M;
                freq = 14000000;
                atualizaDisplay();
                Vfo vfo = new Vfo();
                vfo.setBanda(bandaSelecionada);
                vfo.setFrequencia(freq);
                new DefineFrequencia().execute(vfo);
            }
        });

        btn40m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotao40m();
                bandaSelecionada = BANDA_40M;
                freq = 7000000;
                atualizaDisplay();
                Vfo vfo = new Vfo();
                vfo.setBanda(bandaSelecionada);
                vfo.setFrequencia(freq);
                new DefineFrequencia().execute(vfo);
            }
        });

        btn80m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotao80m();
                bandaSelecionada = BANDA_80M;
                freq = 3500000;
                atualizaDisplay();
                Vfo vfo = new Vfo();
                vfo.setBanda(bandaSelecionada);
                vfo.setFrequencia(freq);
                new DefineFrequencia().execute(vfo);
            }
        });

        btnUSB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotaoUSB();
                modo = "USB";
                new DefineModo().execute(modo);
            }
        });

        btnLSB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotaoLSB();
                modo = "LSB";
                new DefineModo().execute(modo);
            }
        });

        btnFT8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selecionaBotaoUSB();
                modo = "USB";
                new DefineModo().execute(modo);
                if (bandaSelecionada.equals(BANDA_15M)) {
                    freq = 21074000;
                }
                if (bandaSelecionada.equals(BANDA_20M)) {
                    freq = 14074000;
                }
                if (bandaSelecionada.equals(BANDA_40M)) {
                    freq = 7074000;
                }
                if (bandaSelecionada.equals(BANDA_80M)) {
                    freq = 3573000;
                }
                atualizaDisplay();
                Vfo vfo = new Vfo();
                vfo.setBanda(bandaSelecionada);
                vfo.setFrequencia(freq);
                new DefineFrequencia().execute(vfo);
            }
        });

        imgMinusS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq - 100;
                if (calculaLimitesDaBanda()) {
                    new DiminuiFrequencia().execute(100);
                } else {
                    freq = freq + 100;
                }
            }
        });

        imgMinusM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq - 1000;
                if (calculaLimitesDaBanda()) {
                    new DiminuiFrequencia().execute(1000);
                } else {
                    freq = freq + 1000;
                }
            }
        });

        imgMinusL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq - 10000;
                if (calculaLimitesDaBanda()) {
                    new DiminuiFrequencia().execute(10000);
                } else {
                    freq = freq + 10000;
                }
            }
        });

        imgPlusS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq + 100;
                if (calculaLimitesDaBanda()) {
                    new AumentaFrequencia().execute(100);
                } else {
                    freq = freq - 100;
                }
            }
        });

        imgPlusM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq + 1000;
                if (calculaLimitesDaBanda()) {
                    new AumentaFrequencia().execute(1000);
                } else {
                    freq = freq - 1000;
                }
            }
        });

        imgPlusL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freq = freq + 10000;
                if (calculaLimitesDaBanda()) {
                    new AumentaFrequencia().execute(10000);
                } else {
                    freq = freq - 10000;
                }
            }
        });

        imgPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IdentificaBanda().execute(0);
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View CustomView = inflater.inflate(R.layout.dialog_url, null);
        builder.setView(CustomView);
        builder.setMessage("").setTitle(R.string.dialogUrl);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                TextView txtUrl = CustomView.findViewById(R.id.campoUrl);
                if (!txtUrl.getText().toString().equals("")) {
                    String newIp = txtUrl.getText().toString();
                    HttpUtils.setEndpoint(newIp);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("ip", newIp);
                    editor.apply();
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        dialogUrl = builder.create();
    }

    @Override
    protected void onStart() {
        super.onStart();
        new ConsultaFrequenciaAtual().execute(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_url) {
            alteraURL();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Método que permite alterar a URL de conexão à API
     */
    private void alteraURL(){
        Toast.makeText(getApplicationContext(), "IP: "+HttpUtils.getEndpoint(), Toast.LENGTH_LONG).show();
        dialogUrl.show();
    }

    /**
     * Método que atualiza a informação exibida do DIAL com a frequência ativa no momento.
     */
    private void atualizaDisplay(){
        String temp = "" + freq;
        int i = temp.length();
        if (i == 7) {
            String bloco1 = temp.substring(0,1);
            String bloco2 = temp.substring(1,4);
            String bloco3 = temp.substring(4);
            String frequencia = bloco1 + "." + bloco2 + "." + bloco3;
            dial.setText(frequencia);
        } else {
            String bloco1 = temp.substring(0,2);
            String bloco2 = temp.substring(2,5);
            String bloco3 = temp.substring(5);
            String frequencia = bloco1 + "." + bloco2 + "." + bloco3;
            dial.setText(frequencia);
        }
    }

    //não utilizado ainda
    /*private void calculaFrequenciaDoDisplay(){
        String temp = dial.getText().toString();
        temp = temp.replace(".", "");
        freq = Integer.parseInt(temp);
    }*/

    /**
     * Método que modifica a aparencia do botão para identificar qual banda foi selecionada e está ativa no momento.
     */
    private void selecionaBotao15m() {
        int color = Color.parseColor("#84a6cd");
        btn15m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#000000");
        btn20m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn40m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn80m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    private void selecionaBotao20m() {
        int color = Color.parseColor("#84a6cd");
        btn20m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#000000");
        btn15m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn40m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn80m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    /**
     * Método que modifica a aparencia do botão para identificar qual banda foi selecionada e está ativa no momento.
     */
    private void selecionaBotao40m() {
        int color = Color.parseColor("#84a6cd");
        btn40m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#000000");
        btn15m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn20m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn80m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    /**
     * Método que modifica a aparencia do botão para identificar qual banda foi selecionada e está ativa no momento.
     */
    private void selecionaBotao80m() {
        int color = Color.parseColor("#84a6cd");
        btn80m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#000000");
        btn15m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn40m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        btn20m.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    /**
     * Método que modifica a aparencia do botão para identificar qual modo foi selecionado e está ativa no momento.
     */
    private void selecionaBotaoUSB() {
        int color = Color.parseColor("#84a6cd");
        btnUSB.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#303f9f");
        btnLSB.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    /**
     * Método que modifica a aparencia do botão para identificar qual modo foi selecionado e está ativa no momento.
     */
    private void selecionaBotaoLSB() {
        int color = Color.parseColor("#84a6cd");
        btnLSB.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
        color = Color.parseColor("#303f9f");
        btnUSB.getBackground().mutate().setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC));
    }

    /**
     * Método que calcula se a frequência pretendida está dentro dos limites definidos para a banda selecionada.
     * Retorna verdadeiro quando está dentro dos limites e falso quando está fora dos limites.
     * @return boolean
     */
    private boolean calculaLimitesDaBanda() {
        if (bandaSelecionada.equals(BANDA_15M)) {
            if (freq >= 21000000 && freq <= 21450000) {
                return true;
            } else {
                return false;
            }
        } else if (bandaSelecionada.equals(BANDA_20M)) {
            if (freq >= 14000000 && freq <= 14350000) {
                return true;
            } else {
                return false;
            }
        } else if (bandaSelecionada.equals(BANDA_40M)) {
            if (freq >= 7000000 && freq <= 7300000) {
                return true;
            } else {
                return false;
            }
        } else if (bandaSelecionada.equals(BANDA_80M)) {
            if (freq >= 3500000 && freq <= 4000000) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }


    // Classes Assíncronas para realizar as chamadas Http para a API Rest rodando no raspberry.
    // Utiliza a classe utilitária HttpUtils que contém os metodos necessários para realizar as chamadas Http.
    // É necessário utilizar AsyncTask para rodas as chamadas fora da thread principal e evitar mensagens de ANR,
    // Aplicação não está respondendo na tela principal.

    private class DefineFrequencia extends  AsyncTask<Vfo, Object, String> {

        @Override
        protected String doInBackground(Vfo... params) {
            try {
                return HttpUtils.defineFrequencia(params[0]);
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }
    }

    private class DefineModo extends  AsyncTask<String, Object, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                return HttpUtils.defineModo(params[0]);
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resul){
            try {
                JSONObject json = new JSONObject(resul);
                freq = json.getInt("frequencia");
                modo = json.getString("modo");
                atualizaDisplay();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class DiminuiFrequencia extends AsyncTask<Integer, Object, String> {

        @Override
        protected String doInBackground(Integer... params) {
            try {
                return HttpUtils.diminuiFrequencia(params[0]);
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resul){
            try {
                JSONObject json = new JSONObject(resul);
                freq = json.getInt("frequencia");
                atualizaDisplay();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class AumentaFrequencia extends AsyncTask<Integer, Object, String> {

        @Override
        protected String doInBackground(Integer... params) {
            try {
                return HttpUtils.aumentaFrequencia(params[0]);
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resul){
            try {
                JSONObject json = new JSONObject(resul);
                freq = json.getInt("frequencia");
                atualizaDisplay();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class ConsultaFrequenciaAtual extends AsyncTask<Integer, Object, String> {

        @Override
        protected String doInBackground(Integer... params) {
            try {
                return HttpUtils.consultaFrequenciaAtual();
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resul){
            try {
                JSONObject json = new JSONObject(resul);
                freq = json.getInt("frequencia");
                bandaSelecionada = json.getString("banda");
                modo = json.getString("modo");
                atualizaDisplay();
                if (bandaSelecionada.equals(BANDA_15M)) selecionaBotao15m();
                if (bandaSelecionada.equals(BANDA_20M)) selecionaBotao20m();
                if (bandaSelecionada.equals(BANDA_40M)) selecionaBotao40m();
                if (bandaSelecionada.equals(BANDA_80M)) selecionaBotao80m();
                if (modo.equals("USB")) selecionaBotaoUSB();
                if (modo.equals("LSB")) selecionaBotaoLSB();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class IdentificaBanda extends AsyncTask<Integer, Object, String> {

        @Override
        protected String doInBackground(Integer... params) {
            try {
                return HttpUtils.identificaBanda();
            } catch (Exception e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Erro de conexão", Toast.LENGTH_SHORT).show();
                    }
                });
                return null;
            }
        }

        @Override
        protected void onPostExecute(String resul){
            try {
                JSONObject json = new JSONObject(resul);
                String banda = json.getString("banda");
                if (!bandaSelecionada.equals(banda)) {
                    bandaSelecionada = banda;
                    if (bandaSelecionada.equals(BANDA_15M)) {
                        freq = 21000000;
                        btn15m.callOnClick();
                    }
                    if (bandaSelecionada.equals(BANDA_20M)) {
                        freq = 14000000;
                        btn20m.callOnClick();
                    }
                    if (bandaSelecionada.equals(BANDA_40M)) {
                        freq = 7000000;
                        btn40m.callOnClick();
                    }
                    if (bandaSelecionada.equals(BANDA_80M)) {
                        freq = 3500000;
                        btn80m.callOnClick();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}